#!/bin/sh
#
# Copyright (c) 2014, The Board of Trustees of the University of Illinois
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 
# 
# Contributors: HLN Consulting, LLC and other members of the
#               SHARPS DS2 team, http://sharps.org/
# 

#
# opencds_concept_generator.sh
#
# Check to make sure SNOMED CT mapping files are present, then
# update the concept names in the SQLite database to match what
# the OpenCDS Predicate-Reducer expects, then generate fresh OpenCDS 
# concept mapping files and install them into the proper location.
#

PATH_TO_DS2_PREDICATE_REDUCER_PROJECT=/ds2/predicate-reducer

if [ -a ../data/ICD9CM_SNOMED_MAP_1TOM.txt ] && [ -a ../data/ICD9CM_SNOMED_MAP_1TO1.txt ] && [ -a ../data/SNOMEDCT_CORE_SUBSET_201311.txt ] && [ -a ../data/ccs_icd10_2006.csv ] && [ -a ../data/tls_Icd10HumanReadableMap_INT_20131220.tsv ]
then
    dos2unix ../data/ccs_icd10_2006.csv
    dos2unix ../data/tls_Icd10HumanReadableMap_INT_20131220.tsv
    sqlite3 ../database/ds2.db < opencds_db_update.sql
    perl opencds_concept_generator.pl
    rm $PATH_TO_DS2_PREDICATE_REDUCER_PROJECT/opencds-pr-service-data/src/main/resources/conceptMappingSpecifications/manualMappings/ICD9CM_*.xml
    rm $PATH_TO_DS2_PREDICATE_REDUCER_PROJECT/opencds-pr-service-data/src/main/resources/conceptMappingSpecifications/manualMappings/SNOMEDCT_*.xml
    mv ICD9CM_*.xml SNOMEDCT_*.xml $PATH_TO_DS2_PREDICATE_REDUCER_PROJECT/opencds-pr-service-data/src/main/resources/conceptMappingSpecifications/manualMappings
else
    echo "Error: SNOMED CT mapping files not present.  See README.MD for more information."
fi
