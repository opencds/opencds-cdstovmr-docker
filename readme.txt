
You must place the following files in the same folder where Dockerfile is located.  
- cda-ws-ear.ear
- cda-ws-web.war
Terminology resources from: https://uts.nlm.nih.gov/
- ICD9CM_SNOMED_MAP_1TO1.txt
- ICD9CM_SNOMED_MAP_1TOM.txt
- SNOMEDCT_CORE_SUBSET_201311.txt
- tls_Icd10HumanReadableMap_INT_20131220.tsv

Build image :
sudo docker build -t="opencds/cdatovmr" .

Create container :
sudo docker run -p 8080:8080 -p 2222:22 -p 4848:4848 -p 8181:8181 -p 9009:9009 --name cdatovmr -i -t opencds/cdatovmr /bin/bash


Check Inference Analyzer:
http://locahost:8080/SimplePredicateReducerVisualizer-1.0-SNAPSHOT

Check opencds service / predicate reducer web service:
http://localhost:8080/opencds-decision-support-service-1.2.0-SNAPSHOT?wsdl
http://localhost:8080/opencds-decision-support-service-1.2.0-SNAPSHOT/evaluate?wsdl

Check cda web service:
http://localhost:8080/CdaService/Evaluate?wsdl

Check predicate reducer web app:
http://localhost:8080/cda-ws-web/

Run Glassfish in debug mode (port 9009):
cd $GLASSFISH/bin; asadmin start-domain --debug domain1

References
https://sharps-ds2.atlassian.net/wiki/




